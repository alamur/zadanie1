﻿#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <algorithm>
#include <iomanip>
#include <cstdlib>

struct Color {
    int red;
    int green;
    int blue;
    int alpha;

    Color() : red(0), green(0), blue(0), alpha(255) {}  

    Color(int r, int g, int b, int a) : red(r), green(g), blue(b), alpha(a) {}



   
    void display() {
        std::cout << "Red: " << red << " Green: " << green << " Blue: " << blue << " Alpha: " << alpha << std::endl;
        std::cout << "Hex: #" << std::setfill('0') << std::setw(2) << std::hex << red
            << std::setfill('0') << std::setw(2) << std::hex << green
            << std::setfill('0') << std::setw(2) << std::hex << blue
            << std::setfill('0') << std::setw(2) << std::hex << alpha << std::endl;
    }
};


Color mixColors(const std::vector<Color>& colors) {
    int red = 0, green = 0, blue = 0, alpha = 0;
    for (const Color& color : colors) {
        red += color.red;
        green += color.green;
        blue += color.blue;
        alpha += color.alpha;
    }
    red /= colors.size();
    green /= colors.size();
    blue /= colors.size();
    alpha /= colors.size();
    return Color(red, green, blue, alpha);
}


Color findLowest(const std::vector<Color>& colors) {
    Color lowest = colors[0];
    for (const Color& color : colors) {
        if (color.red < lowest.red) {
            lowest.red = color.red;
        }
        if (color.green < lowest.green) {
            lowest.green = color.green;
        }
        if (color.blue < lowest.blue) {
            lowest.blue = color.blue;
        }
        if (color.alpha < lowest.alpha) {
            lowest.alpha = color.alpha;
        }
    }
    return lowest;
}


Color findHighest(const std::vector<Color>& colors) {
    Color highest = colors[0];
    for (const Color& color : colors) {
        if (color.red > highest.red) {
            highest.red = color.red;
        }
        if (color.green > highest.green) {
            highest.green = color.green;
        }
        if (color.blue > highest.blue) {
            highest.blue = color.blue;
        }
        if (color.alpha > highest.alpha) {
            highest.alpha = color.alpha;
        }
    }
    return highest;
}


Color mixSaturate(const std::vector<Color>& colors) {
    int red = 0, green = 0, blue = 0, alpha = 0;
    double hue = 0, saturation = 0, lightness = 0;

    for (const Color& color : colors) {
        red += color.red;
        green += color.green;
        blue += color.blue;
        alpha += color.alpha;
    }

    red /= colors.size();
    green /= colors.size();
    blue /= colors.size();
    alpha /= colors.size();

    return Color(red, green, blue, alpha);
}

int main(int argc, char* argv[]) {
    
    std::string mode = "mix";

    
    std::vector<Color> colors;
    for (int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if (arg == "--mode" || arg == "-m") {
            if (i + 1 < argc) {
                mode = argv[i + 1];
                i++;
            }
        }
        else {
            
            std::istringstream iss(arg);
            int r, g, b, a;
            if (iss >> r >> g >> b >> a) {
                if (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255 && a >= 0 && a <= 255) {
                    colors.emplace_back(r, g, b, a);
                }
            }
        }
    }

   
    std::ifstream file("colors.txt");
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            std::istringstream iss(line);
            int r, g, b, a;
            if (iss >> r >> g >> b >> a) {
                if (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255 && a >= 0 && a <= 255) {
                    colors.emplace_back(r, g, b, a);
                }
            }
        }
        file.close();
    }

    if (colors.empty()) {
        std::cout << "No valid colors found." << std::endl;
        return 1;
    }

    Color result;
    if (mode == "mix") {
        result = mixColors(colors);
    }
    else if (mode == "lowest") {
        result = findLowest(colors);
    }
    else if (mode == "highest") {
        result = findHighest(colors);
    }
    else if (mode == "mix-saturate") {
        result = mixSaturate(colors);
    }
    else {
        std::cout << "Invalid mode specified." << std::endl;
        return 1;
    }

    result.display();

    return 0;
}
